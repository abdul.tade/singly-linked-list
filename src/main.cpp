# include <iostream>
# include "../headers/linkedlist.hpp"


int main(int argc, char *argv[],char* envp[]) {
    
  auto ll = LinkedList<int>{1, 2, 3, 4, 5};
  std::function<int(int,int const&)> f = [](int a,int const& i) {
    return a + i;
  };

  auto r = ll.reduce(f);

  std::cout << "ll: " << ll << std::endl;
  std::cout << "r: "  << r  << std::endl;

  return 0;
}