# include <memory>

template <typename T> struct Node {
public:
  T value;
  std::shared_ptr<Node<T>> next;
};


template <typename T>
using NODE_PTR = std::shared_ptr<Node<T>>;