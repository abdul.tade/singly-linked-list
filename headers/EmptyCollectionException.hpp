
# include <exception>
# include <string>

/**
 * @brief Exception class when trying to perform operation on an empty collection
 * @param message string to print out when exception is thrown
*/
class EmptyCollectionException : public std::exception
{
    private:
        const char * message;

    public:

        EmptyCollectionException()
            : message("Collection is empty")
            {}
        
        EmptyCollectionException(const char* msg) 
            : message(msg)
            {}

        EmptyCollectionException(EmptyCollectionException const&e) = default;

        const char* what() const noexcept  {
            return message;
        }

        virtual ~EmptyCollectionException() = default;
};