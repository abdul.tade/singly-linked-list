#pragma once

#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <mutex>
#include <ostream>
#include <stdexcept>
#include <string>
#include <thread>
#include <optional>
#include <functional>
#include <vector>
#include "node.hpp"
#include "EmptyCollectionException.hpp"

template <typename T> class LinkedList {

public:
  struct Iterator {

  private:
    std::shared_ptr<Node<T>> head;

  public:
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using value_type = T;
    using pointer = T *;
    using reference = T &;

    Iterator(std::shared_ptr<Node<T>> ptr) 
      : head(ptr) 
    {}

    T& operator*() const {
      if (head == nullptr) {
        throw EmptyCollectionException("List is empty");
      }
      return head->value;
    }

    Iterator &operator++() {
      head = head->next;
      return *this;
    }

    Iterator operator++(int) {
      Iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    friend bool operator==(const Iterator &a, const Iterator &b) {
      return a.head == b.head;
    }

    friend bool operator!=(const Iterator &a, const Iterator &b) {
      return a.head != b.head;
    }
  };

  Iterator begin() { return Iterator(this->head); }

  Iterator end() { return Iterator(nullptr); }

private:
  const NODE_PTR<T> empty_node = NODE_PTR<T>();
  std::size_t length = 0;
  std::shared_ptr<Node<T>> head = empty_node;
  std::shared_ptr<Node<T>> tail = empty_node;

public:

  LinkedList() 
  {}

  LinkedList(std::initializer_list<T> list)
  {
    for(auto const& e : list) {
      this->insertEnd(e);
    }
  }

  LinkedList(LinkedList<T>&& ll)
    : length(std::exchange(ll.length,0)),
      head(std::exchange(ll.head,empty_node)),
      tail(std::exchange(ll.tail,empty_node))
  {}

  LinkedList(LinkedList<T> const& ll) = delete;

  virtual ~LinkedList() noexcept = default;

  void insertEnd(const T &value) {

    if (this->length == 0) {

      if(is_empty()) {

        this->head = NODE_PTR<T>(new Node<T>);
        this->head->value = value;
        this->head->next = empty_node;
        this->tail = this->head;
      }

    }

    else {

      auto e = NODE_PTR<T>(new Node<T>);
      e->value = value;
      e->next = empty_node;
  
      if(this->tail == empty_node) {
        this->tail = e;
      }
      else {
        auto intermediate = this->tail;
        intermediate->next = e;
        this->tail = e;
      }
    }

    this->length += 1;
  }

  friend std::ostream &operator<<(std::ostream &oss, LinkedList<T> const &ll) {

    auto h = ll.head;
    
    if(h == ll.empty_node) {
      return oss;
    }

    while (h != ll.empty_node) {
      oss << h->value << " --> ";
      h = h->next;
    }

    return oss;
  }

  void clear() {
    this->~LinkedList();
    this->length = 0;
  }

  std::size_t size() {
     return this->length; 
  }

  void insertBegin(const T &value) {

    auto h = std::shared_ptr<Node<T>>(new Node<T>);
    h->value = value;

    if (is_empty())
    {
      this->insertEnd(value);
      return;
    }

    h->next = this->head;

    if (is_empty()) {
      this->tail = this->head;
    }

    this->head = h;
    this->length += 1;
  }

  T removeBegin() {

    if (is_empty()) {
      throw EmptyCollectionException{"List is empty"};
    }

    NODE_PTR<T> h = this->head;
    this->head = h->next;

    T value = h->value;

    h.reset();
    this->length -= 1;

    return value;
  }

  T removeEnd()
  {

    if (is_empty()) {
      throw EmptyCollectionException("List is empty");
    }

    NODE_PTR<T> penultimate = empty_node;    /* penultimate node */
    NODE_PTR<T> t = tail;          /* tail node */
    NODE_PTR<T> h = head;          /* head node */

    while (h != empty_node)
    {
      if(h == tail) {
          penultimate = h;
          break;
      }
      h = h->next;
    }
    
    penultimate->next = empty_node;
    this->tail = penultimate;
    T value = t->value;
    t.reset();

    this->length -= 1;
    return value;
  }

  LinkedList<T> filter(std::function<bool(const T&)> &predicate) 
  {

    LinkedList<T> ll{};
    NODE_PTR<T> h = head;

    while (h != empty_node)
    {
      if(predicate(h->value)) {
        ll.insertEnd(h->value);
      }
      h = h->next;
    }

    return ll;
  }

  template <typename R>
  LinkedList<R> map(std::function<R(T const& )>& func)
  {
    LinkedList<R> ll{};
    NODE_PTR<T> h = head;

    while (h != empty_node)
    {
      R v = func(h->value);
      ll.insertEnd(v);
      h = h->next;
    }

    return ll;
  }


  /**
   * @brief Accumulates a given value over a function starting with identity
   * @param identity    A value to initialize the accumulator
   * @param accumulator function that accumulates result from previous iteration
   * @return value from repeated accumulation by accumulator
  */
  T reduce(T identity, std::function<T(T, T const&)>& accumulator) {

    insertBegin(identity);
    T result = reduce(accumulator);
    removeBegin();

    return result;
  }

  T reduce(std::function<T(T, T const&)>& accumulator)
  {
    if (is_empty()) {
      throw EmptyCollectionException("List is empty");
    }
    
    T result = head->value;
    for (auto it = ++this->begin(); it != this->end(); ++it)
    {
      result = accumulator(result,*it);
    }

    return result;
  }

  /**
   * @brief find a unique value based on a function, returns
   * first value found else returns std::nullopt
   * @param predicate function used to filter values
   * @return optional that constains the value if it exists otherwise std::nullopt
  */
  std::optional<T> find_unique(std::function<bool(T const&)>& predicate)
  {
    NODE_PTR<T> h = head;

    while (h != empty_node)
    {
      if (predicate(h->value)) {
        return h->value;
      }
      h = h->next;
    }

    return std::nullopt;
  }

  bool is_empty() {
    return this->head == empty_node;
  }
};
