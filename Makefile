CC=clang++
CXXFLAGS=-g -O3 -Wall -std=c++2a
LFLAGS=-lpthread
SHELL=/bin/bash

.PHONY: all build debug run test

build: src/main.cpp

	make create_build_dir
	[ -d build ] && [ -f src/main.cpp ] && \
		$(CC) $(CXXFLAGS) -o build/main.elf \
			src/main.cpp $(LFLAGS)

test: test/TestLinkedList.cpp  test/TestEmptyCollectionException.cpp
 
	make create_build_dir
	$(CC) $(CXXFLAGS) -g -o build/TestLinkedList test/TestLinkedList.cpp
	$(CC) $(CXXFLAGS) -g -o build/TestEmptyCollectionException test/TestEmptyCollectionException.cpp
	@echo "============ ************        Testing LinkedList.cpp            ************ ============"
	./build/TestLinkedList
	@echo "============ ************ Testing TestEmptyCollectionException.cpp ************ ============"
	./build/TestEmptyCollectionException
	

create_build_dir:

	[ -d build ] || mkdir build

run:
	[ -f build/main.elf ] && ./build/main.elf

strip: build/test build/main.elf

	strip build/test
	strip build/main.elf

clean:

	[ -d build/ ] && rm build/*


debug: build/main.elf

	gdb build/main.elf

debug-test: build/test

	gdb build/TestLinkedList
	gdb build/TestEmptyCollectionException
