# define CATCH_CONFIG_MAIN

# include <algorithm>
# include <cstddef>
# include <exception>
# include "../headers/catch.hpp"
# include "../headers/linkedlist.hpp"

TEST_CASE("check if element length is zero for empty list","[LinkedList<int>::length()]") {
    LinkedList<int> ll{};
    REQUIRE(ll.size() == 0);
}

TEST_CASE("check if dereference operator throws error when list is empty",
          "[LinkedList<int>::Iterator::operator*()]") {
    LinkedList<int> ll{};
    bool isError = false;
    try{
        auto res = *(ll.begin());
    } catch(std::exception &e) {
        if(std::string(e.what()) == "List is empty"){
            isError = true;
        }
    }

    REQUIRE(isError);
}

TEST_CASE("check for element inserted at the end when the list is empty","[LinkedList<int>::insertEnd()]"){
    LinkedList<int> ll{};
    ll.insertEnd(10);
    REQUIRE(*ll.begin() == 10);
}

TEST_CASE("check for element inserted at the end when there are elements in the list","[LinkedList<int>::insertEnd()]"){
    LinkedList<int> ll{};

    ll.insertBegin(-20);
    ll.insertBegin(43);
    ll.insertEnd(10);
    ll.insertEnd(-232233);

    REQUIRE(ll.size() == 4);
    
    auto first = ll.begin();
    auto second = ++first;
    auto third = ++first;

    REQUIRE(*third == 10);
}

TEST_CASE("check for element inserted at the beginning","[LinkedList<int>::insertBegin()]") {
    LinkedList<int> ll{};
    ll.insertEnd(100);
    ll.insertBegin(55);
    REQUIRE(*ll.begin() == 55);
}

TEST_CASE("clear method empties the list","[LinkedList<int>::clear()]") {
    LinkedList<int> ll{};
    ll.clear();
    auto it = ll.begin();
    REQUIRE(ll.size() == 0);
    REQUIRE(it == ll.end());
}

TEST_CASE("Order is preserved when items are inserted","[LinkedList<int>::insertBegin()] [LinkedList<int>::insertEnd()]") {
    LinkedList<int> ll{};

    ll.insertEnd(10);
    ll.insertBegin(-100);
    ll.insertEnd(234);
    ll.insertBegin(100);
    ll.insertBegin(-90);

    std::vector<int> expected_values{-90,100,-100,10,234};

    REQUIRE(ll.size() == 5);

    std::size_t indx = 0;
    for(const auto& e : ll)
    {
        REQUIRE(e == expected_values[indx]);
        ++indx;
    }
}

TEST_CASE("Removing an item from the beginning of list","LinkedList<int>::removeBegin()") {
    LinkedList<int> ll{};
    ll.insertEnd(100);
    ll.removeBegin();

    REQUIRE(ll.size() == 0);
}

TEST_CASE("Removing an element from the end of list" ,"[LinkedList<int>::removeEnd()") {
    LinkedList<int> ll{};

    try {
        ll.removeEnd();
    } catch(std::exception &e){
        REQUIRE(std::string(e.what()) == "List is empty");
    }

    ll.insertBegin(100);
    ll.insertEnd(-239);

    auto result = ll.removeEnd();

    REQUIRE(result == -239);

    REQUIRE(ll.size() == 1);

}

TEST_CASE("filter elements based on predicate","[LinkedList<int>::filter()]") {
    LinkedList<int> ll{1,2,3,4,5,6,7};

    std::function<bool(int const &)> p = [](int const& i) { return i%2 == 0; };

    auto result = ll.filter(p);
    auto expected_values = std::vector<int>{2,4,6};
    std::size_t indx = 0;
    for(auto const& e : result)
    {
        REQUIRE(e == expected_values[indx]);
        ++indx;
    }
}

TEST_CASE("map elements of list to another list","[LinkedList<int>::map<>()]") {
    LinkedList<int> ll{1,2,3,4,5,6,7};

    std::function<int(int const&)> m = [](int const& i) { return i*2; };

    auto result = ll.map<int>(m);
    auto expected_values = std::vector<int>{2,4,6,8,10,12,14};
    std::size_t indx = 0;
    for (auto const& e : result)
    {
        REQUIRE(e == expected_values[indx]);
        ++indx;
    }
}

TEST_CASE("Finding a unique value", "[LinkedList<int>::find_unique()]") {
    LinkedList<int> ll{1,2,3,4,5,6};

    std::function<bool(int const&)> p = [](int const& i) {
        return (2*i) == 10;
    };

    std::function<bool(int const&)> p_false = [](int const& i) {
        return (2*i) == 100;
    };

    auto res = ll.find_unique(p);
    auto false_res = ll.find_unique(p_false);

    REQUIRE(res.has_value());
    REQUIRE(res.value() == 5);
    REQUIRE(!false_res.has_value());
}

TEST_CASE("Performing a reduce operation on list","[LinkedList<int>::reduce()]") {
    LinkedList<int> ll{1,2,3,4,5};
    std::function<int(int,int const&)> reducer = [](int accumulator,int const& element) {
        return accumulator + element;
    };

    auto value = ll.reduce(0,reducer);
    auto value_new = ll.reduce(reducer);
    auto expected_value = 15;

    REQUIRE(value == expected_value);
    REQUIRE(value_new == expected_value);
}