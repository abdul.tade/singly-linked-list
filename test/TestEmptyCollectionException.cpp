#define CATCH_CONFIG_MAIN

# include <iostream>
# include "../headers/catch.hpp"
# include "../headers/EmptyCollectionException.hpp"

TEST_CASE("Collection class is throwable","[EmptyCollectionException()]") {
    try {
        throw EmptyCollectionException();
    } catch(EmptyCollectionException &e) {
        REQUIRE(std::string(e.what()) == "Collection is empty");
    }
}